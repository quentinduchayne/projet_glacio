# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pywt
import time
from fonctions_utiles import gradJ_obs_b, gradJ_obs_c, xSIA, prox#A, laplE_b , q , rho_barre ,gradH
import affichage
    
#==============================================================================
# Constantes et paramètres de l'algo proximal 
#==============================================================================

# Constante Lipschitz
L=1.0e4
lmbda=6e3*L 

tol=1e-6
nitermax=4000
niter =0  

#==============================================================================
# Obtention du chemin d'accès du dossier utilisé actuellement
# Définition des booléens de choix
#==============================================================================
import sys
chemin = sys.path[0]

#Variable afficher pour afficher si égale à 1 
AFFICHER=1
#Booléen qui permet de selectionner le jeu de données que l'on veut utiliser
#0-> cas test 
#1-> cas réel (upernarvik)
DATA = 0

#==============================================================================
# Récupération des données préalablement sauvegardées 
#==============================================================================
if DATA==0:
    Cas_test_interp=np.load(chemin+r'\Data_PE\Data_PE_interp1.npz')
    Bed=Cas_test_interp['Bed']
    pasX=float(Cas_test_interp['pasX'] )
    pasY=float(Cas_test_interp['pasY'])
    H=Cas_test_interp['Surface_Elevations']
    cf=Cas_test_interp['C'] #slip−coefficient
    x=Cas_test_interp['X']
    y=Cas_test_interp['Y']
    Masque=(cf==cf)

else : 
    Cas_reel=np.load(chemin+r'\Data\Upinterp.npz')
    Bed=Cas_reel['Bed']
    H=Cas_reel['Surface']
    pasX=float(Cas_reel['pasX'] )
    pasY=float(Cas_reel['pasY'])
    x=Cas_reel['X']
    y=Cas_reel['Y']
    
    Cas_reel_V=np.load(chemin+r'\Data\UpVelinterp.npz')
    Masque=Cas_reel_V['Masque']
    NV=Cas_reel_V['NV']
    Vx_obs=Cas_reel_V['Vx']
    Vy_obs=Cas_reel_V['Vy']
    
    cf=np.load(chemin+r'\Data\C.npy')
    
#=============================================================================
### Calcul des gradients
#=============================================================================
h = H-Bed #hauteur du glacier

dHy,dHx=np.gradient(H,pasX,pasY) #fonction gradient de numpy
NdH=np.sqrt((dHy**2 + dHx**2)) #norme de H

if DATA ==0 : Vx_obs, Vy_obs = xSIA(h,cf,dHx,dHy,NdH) #Vitesses observées

#=============================================================================
### INITIALISATION   
#=============================================================================

#Initialisation de b et C
Bed_depart=H-h.mean( )
NV=np.sqrt(Vx_obs**2+Vy_obs**2 )
#C_depart=0.5e-20*(NV<2e-6)+1.75e-20*(NV>=2e-6)
cf_depart=cf

# Coefficients de normalisation
b_norm=abs(Bed_depart).mean( )
cf_norm=abs(cf_depart).mean( )
ux,uy = xSIA(h.mean( )*Masque,cf_depart,dHx,dHy,NdH)
J_obs_norm =0.5*(np.linalg.norm(ux-Vx_obs)+np.linalg.norm(uy-Vy_obs))**2 #0.5*||u-V_obs||²_2 dans fonction objectif

# Normalisation de b et C
Bed_depart_norm = Bed_depart/b_norm
cf_depart_norm = cf_depart/cf_norm
    
# Passage en base d'ondelette
Beta_depart,slicesb=pywt.coeffs_to_array(pywt.wavedec2(Bed_depart_norm,'db4'))
Gamma_depart,slicesC=pywt.coeffs_to_array(pywt.wavedec2(cf_depart_norm,'db4'))

# Constante Lipschitz
L=1.0e4
lmbda=6e3*L
tol=1e-6
nitermax=4000
niter =0

B=0
#CF=[]
beta0=np.zeros(nitermax) #quelle varible prend la place du alpha 0 ? dimension ?
beta=np.copy(beta0)

#dimentionnement des variables 
J_obs=np.zeros(nitermax)
J_reg=np.zeros(nitermax)
Err_rel=np.zeros(nitermax) #erreur relative
tgradJ_obs_b=np.zeros(nitermax)
#tgradJ_obs_cf=np.zeros(nitermax)
#grad_J=np.zeros(nitermax)
RMSE=np.zeros(nitermax)

#k = np.concatenate((Beta_depart,Gamma_depart)) #Vecteur objectif contenant b et C
k = np.copy(Beta_depart) #Vecteur objectif contenant b et cf
k_true=np.copy(Bed)
 
g=np.zeros(np.shape(k)) # dimension ?
debut=time.time()
while (niter<nitermax) :           
   # PROXIMAL ACCELERE EN COURS 
    t=2/L
    a=0.5*(t+np.sqrt(t**2+4*t*B))

    v=prox(k-g,B)
    w=(B*k+a*v)/(B+a)
    wb=pywt.waverec2(pywt.array_to_coeffs(k,slicesb,"wavedec2"),'db4')*b_norm
    
    h_obj=H-wb
    ux,uy = xSIA(h_obj,cf,dHx,dHy,NdH)
    
    dwb=gradJ_obs_b(h_obj,dHx,dHy,NdH,Vx_obs,Vy_obs,Masque,ux,uy,cf)*b_norm/J_obs_norm
    dwb_array,slicesb=pywt.coeffs_to_array(pywt.wavedec2(dwb,'db4'))

    k=prox(w-lmbda*dwb_array/L,1./L)
    Bed_obj=pywt.waverec2(pywt.array_to_coeffs(k,slicesb,"wavedec2"),'db4')*b_norm
    #on prend la premiere moitié du vecteur objectif équivalent au beta et on le norme
    kbis=np.copy(Bed_obj)
    cf_obj=cf
    h_obj=H-Bed_obj
    ux,uy = xSIA(h_obj,cf_obj,dHx,dHy,NdH)
    ## Gradients normalises
    db=gradJ_obs_b(h_obj,dHx,dHy,NdH,Vx_obs,Vy_obs,Masque,ux,uy,cf_obj)*b_norm/J_obs_norm

    db_array,slicesb=pywt.coeffs_to_array(pywt.wavedec2(db,'db4'))
    
    grad1=db_array #gradE(beta^k+1)
    
    g=g+a*grad1
    B=B+a 
    
    ## Calcul des fonctions coûts
    J_obs[niter]=0.5*(np.linalg.norm (ux-Vx_obs)+np.linalg.norm (uy-Vy_obs))**2
    Err_rel[niter]=(np.linalg.norm(ux-Vx_obs)+np.linalg.norm (uy-Vy_obs))/(np.linalg.norm(Vx_obs)+np.linalg.norm(Vy_obs))
    print(Err_rel[niter])
    tgradJ_obs_b [niter]=np.linalg.norm(db)
    J_reg[niter] = np.sum(abs(k))
    RMSE[niter]=np.linalg.norm(kbis-k_true,2)/np.linalg.norm(k_true,2)

    if (Err_rel[niter]< tol) :
        print("Critère d'arrêt vérifié")
        break
    niter+=1
fin=time.time()
t=fin-debut
if AFFICHER:
    n=256
    xi,yi = np.meshgrid(x, y)
    b=pywt.waverec2(pywt.array_to_coeffs(k,slicesb,"wavedec2"),'db4')*b_norm

    s=H
    dh=H-b
    #Normalisation des fonctions coûts
    J_obs=J_obs/J_obs[0]
    J_reg=J_reg/J_reg[0]
    tgradJ_obs_b=tgradJ_obs_b/tgradJ_obs_b[0]
    
    affichage.afficher2D(b,"Pente à la surface")
    affichage.afficher2D(Bed,"Pente à la surface théorique")
    affichage.plot_multiple([J_obs, J_reg ,tgradJ_obs_b,RMSE],np.array(["J_obs","J_reg","gradJ","RMSE"]))