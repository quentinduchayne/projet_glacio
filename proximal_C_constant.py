# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pywt
import time
from fonctions_utiles import gradE_b , gradE_c, xSIA#A, laplE_b , q , rho_barre ,gradH
import affichage

#Rappel de la fonction proximal
def prox (beta,t):
    #beta est la matrice qu'on utilise pour le proximal, soit que beta, soit que gamma 
    #soit beta+gamma
    #t le pas de descente
    return np.sign(beta)*(abs(beta)-t)*(abs(beta)>t)
    
#==============================================================================
# Obtention du chemin d'accès du dossier utilisé actuellement
#==============================================================================
import sys
chemin = sys.path[0]

#Variable afficher pour afficher si égale à 1 
AFFICHER=1

#==============================================================================
# Récupération des données préalablement sauvegardées 
#==============================================================================
Cas_test_interp=np.load(chemin+r'\Data_PE\Data_PE_interp1.npz')
Bed=Cas_test_interp['Bed']
pasX=float(Cas_test_interp['pasX'] )
pasY=float(Cas_test_interp['pasY'])
H=Cas_test_interp['Surface_Elevations']
C=Cas_test_interp['C'] #slip−coefficient
x=Cas_test_interp['X']
y=Cas_test_interp['Y']
Masque=(C==C)

h = H-Bed #hauteur du glacier

dHy,dHx=np.gradient(H,pasX,pasY) #fonction gradient de numpy
NdH=np.sqrt((dHy**2 + dHx**2)) #norme de H

Vx_obs, Vy_obs = xSIA(h,C,dHx,dHy,NdH) #Vitesses observées

#Expression en base d'ondelettes
Beta,slicesb=pywt.coeffs_to_array(pywt.wavedec2(Bed,'db4'))
Gamma,slicesC=pywt.coeffs_to_array(pywt.wavedec2(C,'db4'))

#=============================================================================
### INITIALISATION   
#=============================================================================

#Initialisation de b et C constant
Bed_depart=H-h.mean( )
C_depart=C

# Coefficients de normalisation ?????????????
b_norm=abs(Bed_depart).mean( )
C_norm=abs(C_depart).mean( )
ux,uy = xSIA(h.mean( )*Masque,C_depart,dHx,dHy,NdH)
E_norm =0.5*(np.linalg.norm(ux-Vx_obs)+np.linalg.norm(uy-Vy_obs))**2 #0.5*||u-V_obs||²_2 dans fonction objectif

# Normalisation de b et C
Bed_depart_norm = Bed_depart/b_norm
C_depart_norm = C_depart/C_norm
    
# Passage en base d'ondelette
Beta_depart,slicesb=pywt.coeffs_to_array(pywt.wavedec2(Bed_depart_norm,'db4'))
Gamma_depart,slicesC=pywt.coeffs_to_array(pywt.wavedec2(C_depart_norm,'db4'))

# Constante Lipschitz
L=1.0e4
lmbda=6e3*L
tol=1e-6
nitermax=4000
niter =0

E=np.zeros(nitermax)
N=np.zeros(nitermax)
Err_rel=np.zeros(nitermax) #erreur relative
grad_E_b=np.zeros(nitermax)
grad_E_C=np.zeros(nitermax)
grad_E=np.zeros(nitermax)
BG = Beta_depart #Vecteur objectif contenant b et C
t=np.zeros(nitermax)
debut=time.time()   
while (niter<nitermax) :
    Bed_obj=pywt.waverec2(pywt.array_to_coeffs(BG,slicesb,"wavedec2"),'db4')*b_norm
    #on prend la premiere moitié du vecteur objectif équivalent au beta et on le norme
    C_obj=C

    h_obj=H-Bed_obj
    ux,uy = xSIA(h_obj,C_obj,dHx,dHy,NdH)
    
    ## Gradients normalises
    db=gradE_b(h_obj,dHx,dHy,NdH,Vx_obs,Vy_obs,Masque,ux,uy,C_obj)*b_norm/E_norm
    db_array,slicesb=pywt.coeffs_to_array(pywt.wavedec2(db,'db4'))

    ## Prox
    BG_plus=prox(BG-lmbda*db_array/L,1./L)
        
    ## Calcul des fonctions coûts
    E[niter]=0.5*(np.linalg.norm (ux-Vx_obs)+np.linalg.norm (uy-Vy_obs))**2
    Err_rel[niter]=(np.linalg.norm(ux-Vx_obs)+np.linalg.norm (uy-Vy_obs))/(np.linalg.norm(Vx_obs)+np.linalg.norm(Vy_obs))
    print(Err_rel[niter])
    grad_E_b [niter]=np.linalg.norm(db)
    N[niter] = np.sum(abs(BG))

    if (Err_rel[niter]< tol) :
        print("Critère d'arrêt vérifié")
        break
    niter+=1
    BG=np.copy(BG_plus)
fin=time.time()
t=fin-debut 
if AFFICHER:
    n=256
    xi,yi = np.meshgrid(x, y)
    b=pywt.waverec2(pywt.array_to_coeffs(BG,slicesb,"wavedec2"),'db4')*b_norm
    c=C_depart_norm
    s=H
    dh=H-b
    affichage.afficher_bed_surf(b,s,x,y)
    affichage.afficher2D(b,"Pente à la surface")
    affichage.afficher2D(c,"coefficient de friction")
    affichage.afficher2D(Bed,"Pente à la surface théorique")
    affichage.afficher2D(C,"coefficient de friction théorique")