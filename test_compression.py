#! /usr/bin/env python2
# -*- coding: utf-8 -*-
'''
    File name: testCompression.py
    Author: Nicolas Gonzalez, inspiré du code de Roxane Lassis et Victor Minard
    Description : Test de la compression avec les ondelettes db2 et db4
    Contexte : projet de 4e année à l'INSA Toulouse
    Date created: 04/2017
    Date last modified: 18/05/2017
    Python Version: 2.7
'''

import numpy as np
import affichage
import matplotlib.pyplot as plt
import pywt
import scipy.io as sio

#list_complète = ['bior1.1', 'bior1.3', 'bior1.5', 'bior2.2', 'bior2.4', 'bior2.6', 'bior2.8', 'bior3.1', 'bior3.3', 'bior3.5', 'bior3.7', 'bior3.9', 'bior4.4', 'bior5.5', 'bior6.8', 'coif1', 'coif2', 'coif3', 'coif4', 'coif5', 'coif6', 'coif7', 'coif8', 'coif9', 'coif10', 'coif11', 'coif12', 'coif13', 'coif14', 'coif15', 'coif16', 'coif17', 'db1', 'db2', 'db3', 'db4', 'db5', 'db6', 'db7', 'db8', 'db9', 'db10', 'db11', 'db12', 'db13', 'db14', 'db15', 'db16', 'db17', 'db18', 'db19', 'db20', 'db21', 'db22', 'db23', 'db24', 'db25', 'db26', 'db27', 'db28', 'db29', 'db30', 'db31', 'db32', 'db33', 'db34', 'db35', 'db36', 'db37', 'db38', 'dmey', 'haar', 'rbio1.1', 'rbio1.3', 'rbio1.5', 'rbio2.2', 'rbio2.4', 'rbio2.6', 'rbio2.8', 'rbio3.1', 'rbio3.3', 'rbio3.5', 'rbio3.7', 'rbio3.9', 'rbio4.4', 'rbio5.5', 'rbio6.8', 'sym2', 'sym3', 'sym4', 'sym5', 'sym6', 'sym7', 'sym8', 'sym9', 'sym10', 'sym11', 'sym12', 'sym13', 'sym14', 'sym15', 'sym16', 'sym17', 'sym18', 'sym19', 'sym20']
# Liste des bases d'ondelettes que l'on veut tester 
LISTE_TEST = np.array(['db2','db4', 'bior1.3', 'bior1.5'])
# Variable sur laquelle on veut tester la compression: 'C' ou 'Bed'
VAR_TEST = 'C'

#==============================================================================
# Obtention du chemin d'accès du dossier utilisé actuellement
#==============================================================================
import sys
chemin = sys.path[0]

#==============================================================================
# Import des données d'Upernavik
#==============================================================================

#Données interpolées
UpMCinterp=np.load(chemin+r'\Data\Upinterp.npz')
X=UpMCinterp['X']
Y=UpMCinterp['Y']
Masque=UpMCinterp['Masque']
NV=np.load(chemin+r'\Data\UpVelinterp.npz')['NV']


if VAR_TEST != 'C' : var=UpMCinterp[VAR_TEST]
else : var = np.load(chemin+r'\Data\C.npy')
  
var0=np.copy(var)
var0[Masque==0]=0
M,N=var.shape

#Initialisation des listes 
if VAR_TEST == 'C' : 
    LISTE_TEST = np.concatenate( (np.array(['original']),LISTE_TEST) )
    Tab_var_recomp = [var0]
    Tab_erreurs = [0]
    Tab_erreurs_canyons = [0]
    #indice qui donne l'emplacement du début des données compressées
    ind_debut = 1
else : 
    var_orig=sio.loadmat(chemin+r'\UpernavikData\UpernavikMC.mat')[VAR_TEST]

    LISTE_TEST = np.concatenate( (np.array(['original','interprete']),LISTE_TEST) )
    Tab_var_recomp = [var_orig,var0]
    Tab_erreurs = [0,0]
    Tab_erreurs_canyons = [0,0]
    #indice qui donne l'emplacement du début des données compressées
    ind_debut = 2

#Définition d'un masque pour les zones à "grande vitesse"
MasqueNV = (NV>1e3)

#==============================================================================
# Test de la compression des différentes bases d'ondelettes voulues 
#==============================================================================
for n in LISTE_TEST[ind_debut:]:
    #décomposition
    var_decomp= pywt.wavedec2(var0,n)
    var_decomp_array,b=pywt.coeffs_to_array(var_decomp) #passage en format matrice
    
    #Supression des 90% des valeurs les plus faibles 
    q=np.percentile(abs(var_decomp_array),90) #Determination du 0.9 quantile 
    var_decomp_array=(abs(var_decomp_array)>q)*var_decomp_array

    #Recomposition
    var_decomp=pywt.array_to_coeffs(var_decomp_array,b,'wavedec2') #"eclatement" de la matrice
    var_recomp = pywt.waverec2(var_decomp,n) #retour en base canonique
    
    #Calcul des errreurs
    Tab_erreurs.append(np.linalg.norm(var_recomp-var0)/np.linalg.norm(var0))  #erreur relative globale
    #calcul de l'erreur relative sur les zones à "grande vitesse"
    Tab_erreurs_canyons.append( np.linalg.norm(var_recomp[MasqueNV]-var0[MasqueNV])/np.linalg.norm(var0[MasqueNV]) )
    
    var_recomp[Masque==0]=np.nan 
    Tab_var_recomp.append(var_recomp)


#==============================================================================
# Affichages
#==============================================================================

print('Erreur sur l\'enssemble du domaine :')
for i in range(ind_debut,len(LISTE_TEST)): print('Erreur sur ' + LISTE_TEST[i] ,Tab_erreurs[i]*100,'%')

print('\nErreur au niveau des canyons :')
for i in range(ind_debut,len(LISTE_TEST)): print('Erreur sur ' + LISTE_TEST[i] ,Tab_erreurs_canyons[i]*100,'%')


#Cf script affichage - affiche la var originales et celles recomposés sur le même 
#subplot
affichage.affichage_multiple_2D(Tab_var_recomp,LISTE_TEST)

if VAR_TEST == 'Bed' : 

    L2=[]
    for i in range(2,len(LISTE_TEST)):
        L2.append(Tab_var_recomp[i][:,85])
    
    affichage.plot_multiple(L2,LISTE_TEST[2:],superposition=var[:,85],nom_superposition="Original",message="Compression a 90% avec ")
    
    # affiche l'enplacement des canyons 
    plt.figure()
    plt.plot(var[:,85],label='Original')
    BRM=(MasqueNV*var)[:,85]
    BRM[BRM==0]=np.nan
    plt.plot((BRM),'r', label='Canyons',lw=2)
    plt.legend(loc=0)
    
    
    plt.figure()
    plt.imshow(var)
    MasqueNV2=1.*MasqueNV
    MasqueNV2[MasqueNV==0]=np.nan
    MasqueNV2[0,0]=-20
    plt.imshow(MasqueNV2,cmap='Greys')
    plt.title('Zone Canyons')

plt.show()
