#! /usr/bin/env python2
# -*- coding: utf-8 -*-


import numpy as np

################################################################################
                       #### Définition des paramètres ####
################################################################################
L=100. #dimension du domaine en km

A=3.e-24 #rate factor de la glace... ? A=1/(2*mu0)**q, mu0 la viscosité. ## modif :A=3.e-24
q=3. #Valeur usuelle pour les glaciers.
#Valeur de A,q constantes car flux supposé isotherme

rho=934. #densité de la glace
g=9.81 #accélération de la gravité
rho_barre=(rho*g)**q

################################################################################


################################################################################
                               #### xSIA ####
################################################################################
def xSIA(h,C,dHx,dHy,NdH,A=A,rho_barre=rho_barre,q=q):
    """
    Donne la vitesse établie par le modèle physique xSIA explicite à partir de :
    h la profondeur
    C le coefficient de friction
    dHx, dHy les gradients de l'élévation H, et NdH leur norme
    """
    S=rho_barre*(NdH**(q-1))*(C+2*A*h/(q+1))*(h**q)
    ux=S*dHx
    uy=S*dHy
    return ux, uy
################################################################################
    
################################################################################
                              ### gradE ###
################################################################################

def gradJobs_b(h,dHx,dHy,NdH,Ux_obs,Uy_obs,Masque,Ux,Uy,C,A=A,rho_barre=rho_barre,q=q):
    """
     fonction de Calcul du gradient de E par rapport à b (C constant)
     h la profondeur
     dHx, dHy les gradients de l'élévation H, et NdH leur norme
     Ux_obs et Uy_obs les vitesses observées
     Masque un masque indiquant où sont les données
     Ux, Uy les vitesses calculées via xSIA
     C le coefficient de friction
    """
    gradu =- rho_barre*(NdH**(q-1))*(h**(q-1))*(C*q + 2*A*h) 
    DEx=gradu*(Ux-Ux_obs)*dHx
    DEy=gradu*(Uy-Uy_obs)*dHy
    DE=Masque*(DEx+DEy)
    return DE


def gradJobs_c(h,dHx,dHy,NdH,Ux_obs,Uy_obs,Masque,Ux,Uy,C,A=A,rho_barre=rho_barre,q=q):
    """
    fonction de Calcul du gradient de E par rapport à C (b constant)
    peut servir pour le calcul de la constante de Lipschitz
    h la profondeur
    dHx, dHy les gradients de l'élévation H, et NdH leur norme
    Ux_obs et Uy_obs les vitesses observées
    Masque un masque indiquant où sont les données
    Ux, Uy les vitesses calculées via xSIA
    C le coefficient de friction
    """
    gradu =rho_barre*(h**q)*(NdH**(q-1))
    DEx=gradu*(Ux-Ux_obs)*dHx
    DEy=gradu*(Uy-Uy_obs)*dHy
    DE=Masque*(DEx+DEy)
    return DE
 ################################################################################
    
################################################################################
                              ### laplE_b ###
################################################################################

def laplJobs_b(h,dHx,dHy,NdH,Ux_obs,Uy_obs,Masque,Ux,Uy,C,A=A,rho_barre=rho_barre,q=q):
    """
     fonction de Calcul du laplacien de E par rapport à b (C constant)
     peut servir pour le calcul de la constante de Lipschitz
     h la profondeur
     dHx, dHy les gradients de l'élévation H, et NdH leur norme
     Ux_obs et Uy_obs les vitesses observées
     Masque un masque indiquant où sont les données
     Ux, Uy les vitesses calculées via xSIA
     C le coefficient de friction
    """
    laplu = rho_barre*(NdH**(q-1))*(h**(q-2))*(C*q*(q-1) + 2*A*h*q) 
    DEx=laplu*(Ux-Ux_obs)*dHx
    DEy=laplu*(Uy-Uy_obs)*dHy
    DE=Masque*(DEx+DEy)
    return (gradE_b(h,dHx,dHy,NdH,Ux_obs,Uy_obs,Masque,Ux,Uy,C))**2+DE
 
################################################################################
    
################################################################################
                              ### gradH ###
################################################################################   
    
def grad_H(H,Masque,pasX,pasY):
    """Calcul le gradient de H """
    dHy , dHx=np.gradient(H, pasX , pasY)
    NdH=np.sqrt( ( dHy**2 + dHx**2 ) )
    return dHx,dHy,NdH
    

##############################################################################

################################################################################
                               ### prox ###
################################################################################

def prox(beta,t):
    """
    fonction proximale pour l'algorithme de descente
    beta la variable sur laquelle s'applique le prox
    t le pas de descente
    """
    return np.sign(beta)*(abs(beta)-t)*(abs(beta)>t)

################################################################################

