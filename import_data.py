#! /usr/bin/env python2
# -*- coding: utf-8 -*-
'''
    File name: importData.py
    Author: Quetin Villegas, Quentin Duchayne, Nicolas Gonzalez 
    Description : Module d'import des donnees d'Upernavik, necessite les fichiers UpernavikMC.mat et UpernavikVel.mat 
                  Sauvegarde les données interpolées dans le même fichier que celui où elles se trouvent 
    Contexte : projet de 4e annee a l'INSA Toulouse
    Date created: 02/2018
    Date last modified: 13/02/2018
    Python Version: 2.7
'''
import affichage
import numpy as np
import scipy.io as sio
from scipy.interpolate import griddata

n=256 # Taille souhaite pour les donnees interpolees

###############################################################################
# Obtention du chemin d'accès du dossier utilisé actuellement
###############################################################################
import sys
chemin = sys.path[0]

print(chemin)

############################ GetBed #######################################
# On va récupérer depuis UpernavikMC le Bed, la surface, la masque du domaine
# et la grille de coordonées qui vont avec  

#==============================================================================
# Importation des donnees UpernavikMC
#==============================================================================
print("Importation de UpernavikMC")

UpernavikData=sio.loadmat(chemin+r'\UpernavikData\UpernavikMC.mat')
Bed=UpernavikData['Bed']
X=UpernavikData['X']
Y=UpernavikData['Y']
S = UpernavikData['Surface']

M,N=Bed.shape

affichage.afficher_bed_surf2(Bed,S,X,Y)

#==============================================================================
# Adaptation des donnees
#==============================================================================
B=np.reshape(Bed,M*N,1)
S=np.reshape(S,M*N,1)

X=np.reshape(X,M*N,1)
Y=np.reshape(Y,M*N,1)
Z=np.array([X,Y]).transpose()

#==============================================================================
# Définition d'une nouvelle grille spaciale à partir des limites du domaine 
# et du nombre de points de discrétisation voulus 
# pax, pasy, x, y, les coordonnees et pas de discretisation
#==============================================================================
Xmin=X.min()
Ymin=Y.min()
Xmax=X.max()
Ymax=Y.max()

x=np.linspace(Xmin,Xmax,n)
y=np.linspace(Ymin,Ymax,n)
xi,yi = np.meshgrid(x, y)

pasx=xi[0,1]-xi[0,0]
pasy=yi[1,0]-yi[0,0]

#==============================================================================
# Initialisations
# Genere des donnees a la taille n
# b la bathymetrie 
# s la surface libre
#==============================================================================
b=np.zeros((n,n))
s=np.zeros((n,n))

#==============================================================================
# Interpolation des donnees aux nouvelles coordonnees
# le Masque definit le domaine d'Upernavik
#==============================================================================
print("Interpolation des données de bathimétrie...")
b = griddata(Z,B,(xi,yi))
s = griddata(Z,S,(xi,yi))
print("Interpolation bathimétrie OK")

Masque=np.isfinite(b)

#==============================================================================
# Sauvegarde des donnees interpolees
#==============================================================================

affichage.afficher_bed_surf(b,s,x,y)
print("Sauvegarde")
np.savez(chemin+r'\Data\Upinterp.npz',Bed=b,Surface=s,Masque=Masque,pasX=pasx,pasY=pasy,X=x,Y=y)
print("Sauvegarde OK") 


############################ GetVitesse #######################################
# On va récupérer depuis UnpernavikVel les champs de vitesse, leur normes et
# les pas de discretisation

#==============================================================================
# Importation des donnees UpernavikVel
#==============================================================================
print("Importation de UpernavikVel")

UpernavikVel=sio.loadmat(chemin+r'\UpernavikData\UpernavikVel.mat')
x=UpernavikVel['x'][:,0]
y=UpernavikVel['y'][:,0]
vx=UpernavikVel['vx'][:,0]
vy=UpernavikVel['vy'][:,0]

z=np.array([x,y]).transpose()

#==============================================================================
# Nouvelles coordonnees
#==============================================================================
xmin=x.min()
ymin=y.min()
xmax=x.max()
ymax=y.max()

xi=np.linspace(xmin,xmax,n)
yi=np.linspace(ymin,ymax,n)

xi,yi = np.meshgrid(xi, yi)
pasx=xi[0,1]-xi[0,0]
pasy=yi[1,0]-yi[0,0]

#==============================================================================
# Initialisations
# Genere des donnees de taille n
#==============================================================================
Vx=np.zeros((n,n))
Vy=np.zeros((n,n))

#==============================================================================
# Interpolation des donnees aux nouvelles coordonnees
#==============================================================================
print("Interpolation des données de vitesse...")
Vx = griddata(z,vx,(xi,yi))
Vy = griddata(z,vy,(xi,yi))
print("Interpolation Vitesses OK")

Vx[(Masque==0)]=float('nan')
Vy[(Masque==0)]=float('nan')

NV=((Vx*Masque)**2+(Vy*Masque)**2)**.5 

#==============================================================================
# Sauvegarde des donnees interpolees
#==============================================================================
print("Sauvegarde")
np.savez(chemin+r'\Data\UpVelinterp.npz',Vx=Vx,Vy=Vy,NV=NV,Masque=Masque,pasX=pasx,pasY=pasy);






####### TRES BIZARRE A VOIR CE QUE ON EN FAIT PLUS TARD #######################


########################## GetC ###############################################
# Generation de C le coefficient de friction
print("Generation de C le coefficient de friction")

h=s-b;
Cref=h*1e-18/2;

print(np.linalg.norm(NV), NV.max(), NV.min(), np.log10(1e-23))

C=np.zeros(NV.shape)
#C = (NV<300)*Cref
#C = C + (NV<1000)*(NV>=300)*10*Cref
#C = C + 30*Cref*(NV<2500)*(NV>=1000)
#C = C + 50*Cref*(NV>2500)
C[NV<300]=Cref[NV<300];
C[(NV<1000)*(NV>=300)]=Cref[(NV<1000)*(NV>=300)]*10;
C[(NV<2500)*(NV>=1000)]=Cref[(NV<2500)*(NV>=1000)]*30;
C[NV>2500]=Cref[NV>2500]*50;
#C[NV<1]=1e-23
#C[(1<=NV)*(NV<=10)]=10**-21.5
#C[NV>10]=1e-20
#C[Masque==0]=np.nan

#==============================================================================
# Sauvegarde du C calcule
#==============================================================================
np.save(chemin+r'\Data\C',C);