# -*- coding: utf-8 -*-
'''
    File name:          importDataAc.py
    Author:             Quentin DICHAYNE, Nicolas GONZALEZ et Quentin VILLEGAS
    Description :       Module d'import des données du cas test académique,
                        nécéssite les fichiers bed.npy, C.npy, Surface_Elevations.npy, Surface_Slopes.npy et coordinates.npy
    Contexte :          Projet 4A INSA Toulouse
    Date created:       02/2018
    Date last modified: 06/02/2018
    Python Version:     3.0
'''

import numpy as np
import affichage
from scipy.interpolate import griddata

n=256 # Taille souhaite pour les donnees interpolees
AFFICHER = 1 #On donne 1 si on veut afficher les résultats interpolés 

#==============================================================================
# Obtention du chemin d'accès du dossier utilisé actuellement
#==============================================================================
import sys
chemin = sys.path[0]

#==============================================================================
# Importation des données
#==============================================================================

Bed=np.load(chemin+r'\Data_PE\bed.npy')
C=np.load(chemin+r'\Data_PE\C.npy')
H=np.load(chemin+r'\Data_PE\Surface_Elevations.npy')
dH=np.load(chemin+r'\Data_PE\Surface_Slopes.npy')
Coord=np.load(chemin+r'\Data_PE\coordinates.npy')

print("Importation des données OK")

#==============================================================================
# Définition de la grille spaciale à partir des limites du domaine 
# et du nombre de points de discrétisation voulus 
#==============================================================================
Xmin=Coord.transpose()[0].min()
Ymin=Coord.transpose()[1].min()
Xmax=Coord.transpose()[0].max()
Ymax=Coord.transpose()[1].max()

x=np.linspace(Xmin,Xmax,n)
y=np.linspace(Ymin,Ymax,n)
xi,yi = np.meshgrid(x, y)
pasx=float(xi[0,1]-xi[0,0])
pasy=float(yi[1,0]-yi[0,0])

#==============================================================================
# Initialisation des variables physiques (matrices de taille n) dans 
# notre système de coordonnées :
# b -> bathymetrie
# s -> surface libre
# pax, pasy, x, y, -> coordonnees et pas de discretisation
#==============================================================================
b=np.zeros((n,n))
s=np.zeros((n,n))
c=np.zeros((n,n))
dh=np.zeros((n,n))

#==============================================================================
#Interpolation
#==============================================================================
print("Interpolation des données...")
b = griddata(Coord,Bed,(xi,yi))
s = griddata(Coord,H,(xi,yi))
c = griddata(Coord,C,(xi,yi))
dh = griddata(Coord,dH,(xi,yi))
print("Interpolation des données OK")


#==============================================================================
# Sauvegarde des données interpolées
#==============================================================================

np.savez(chemin+r'\Data_PE\Data_PE_interp1.npz',Bed=b,Surface_Elevations=s,Surface_Slopes=dh,C=c,pasX=pasx,pasY=pasy,X=x,Y=y)
print("Sauvegarde OK") 

if AFFICHER:
    affichage.afficher_bed_surf(b,s,x,y)
    affichage.afficher2D(dh,"Pente à la surface")
    affichage.afficher2D(c,"coefficient de friction")
    