%
% Data post-treated by Mathieu Morlighem, UCI
%
% Upernavik glaciers Greenland (see eg. Google maps).
%
- Fichier matlab flight lines et grids de surface bed epaisseur: 
ProcessedTracks.mat
UpernavikMC.mat

- Figure avec l’épaisseur MC et flight lines
Thickness_and_Tracks.pdf

- Figure vitesse (avec et sans Google maps en dessous):
Upernavik/Vel.pdf
Upernavik/Vel2.pdf

- Figure du Bed MC:
Upernavik/Bed.pdf

