#! /usr/bin/env python2
# -*- coding: utf-8 -*-
'''
    File name: importData.py
    Author: Quetin Villegas, Quentin Duchayne, Nicolas Gonzalez 
    Description : On a des résultats bizarres suite à la récupération des données
                  depuis les fichiers.mat, fichier fait pour réaliser quelques
                  test 
    Contexte : projet de 4e annee a l'INSA Toulouse
    Date created: 02/2018
    Date last modified: 13/02/2018
    Python Version: 2.7
'''
import matplotlib.pyplot as plt
import scipy.io as sio
from mpl_toolkits.mplot3d import Axes3D

n=256 # Taille souhaite pour les donnees interpolees

###############################################################################
# Obtention du chemin d'accès du dossier utilisé actuellement
###############################################################################
import sys
chemin = sys.path[0]

print(chemin)

############################ GetBed #######################################
# On va récupérer depuis UpernavikMC le Bed, la surface, la masque du domaine
# et la grille de coordonées qui vont avec  

#==============================================================================
# Importation des donnees UpernavikMC
#==============================================================================
print("Importation de UpernavikMC")

UpernavikData=sio.loadmat(chemin+r'\UpernavikData\UpernavikMC.mat')
Bed=UpernavikData['Bed']
X=UpernavikData['X']
Y=UpernavikData['Y']
S = UpernavikData['Surface']

M,N=Bed.shape

# On peut voir que dans l'affichage des données récupérées la pluspart des 
# valeurs de Bed et Surface sont des nan  
print(UpernavikData)

#Pourtant l'affichage 2D marche bien
plt.clf()
plt.figure("Surface")
plt.imshow(S)
plt.colorbar()
plt.show()
# 
# plt.clf()
# plt.figure("test")
# plt.imshow(Bed)
# plt.colorbar()
# plt.show()

#Mais pas le 3d    
figb=plt.figure('3d',figsize=(15,5))
ax = Axes3D(figb)
surf=ax.plot_surface(X,Y,S, cmap='ocean', linewidth=0, antialiased=False)
figb.colorbar(surf, shrink=0.5, aspect=20).set_label("bed")
plt.show()  