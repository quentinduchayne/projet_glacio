# -*- coding: utf-8 -*-
'''
    File name: affichage.py
    Author: Nicolas Gonzalez
    Description : Enssemble de fonctions qui vont servir à l'affichage des données 
    Contexte : projet de 4e année à l'INSA Toulouse
    Date created: 02/2018
    Date last modified: 20/02/2018
    Python Version: 3.5
'''

import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


#-------------------------------------------------------------------------------

#Prend une matrice (mat) et un string (nom) et affiche la matrice avec un 
#plt.show tout en nommant la figure obtenur avec "nom" 
def afficher2D(mat, nom):
    plt.figure(nom)
    plt.imshow(mat)
    plt.colorbar()
    plt.show()
 
#-------------------------------------------------------------------------------   
 
 
#-------------------------------------------------------------------------------
                
#DOIT ETRE UTILISE POUR AU MOINS 3 ELEMENTS 
#Prends en entrée une liste de matrices et un tableau de nom correspondant aux 
# matrices, affiche l'enssemble des matrices sur un même subplot à partir de 
# plt.imshow()   
def affichage_multiple_2D(liste_val,tab_nom,sharex_ = False, sharey_=False):   
    #calcul du nombre de lignes et de colonnes dont on a besoin pour afficher 
    #toutes les matrices
    if np.sqrt(len(tab_nom)) - int(np.sqrt(len(tab_nom))) >=0.5 : 
        nb_lignes=int(np.sqrt(len(tab_nom))) +1
        nb_col = nb_lignes   
    else :
        nb_lignes=int(np.sqrt(len(tab_nom)))
        nb_col = nb_lignes + 1    
    
    #initialisation du subplot
    f, axarr= plt.subplots(nb_lignes,nb_col, sharex=sharex_,sharey=sharey_,figsize=(15,15))
    f.subplots_adjust(left = 0.2, bottom = 0.2,
                            right = 0.8, top = 0.8, wspace = 0.2, hspace = 0.4)
    #indices qui vont nous permettre de parcourrir la matrice contenant les axes 
    #dans le bon ordre 
    ind1 = 0
    ind2 = 0
    
    #boucle d'affichage 
    for i in range(len(tab_nom)):
        #si on est pas au bout d'une ligne on continue d'afficher sur les 
        #les colonnes suivantes  
        if ((ind2 + 1) % nb_col != 0) :
            im = axarr[ind1,ind2].imshow(liste_val[i])
            axarr[ind1,ind2].set_title(tab_nom[i])
            ind2 += 1
        #si on se trouve au bout de la ligne on change de ligne et on 
        #réinitialise l'indice des colonnes     
        else :
            im = axarr[ind1,ind2].imshow(liste_val[i])
            axarr[ind1,ind2].set_title(tab_nom[i])
            ind1 += 1
            ind2 = 0
                 
    f.colorbar(im, ax=axarr.ravel().tolist())
    
    plt.show()
 
#-------------------------------------------------------------------------------   
 
 
#-------------------------------------------------------------------------------
 
#NE MARCHE PAS POUR PLUS DE 9 ELEMENTS 
#Prends en entrée une liste de matrices, un tableau de nom correspondant aux 
# matrices, potentillement une autre matrice à afficher en superposition de celles
# de la liste ainsi que son nom et enfin un message à afficher avant le titre du 
# subplot. Affiche l'enssemble des matrices sur un même subplot à partir de 
# plt.plot()    
def plot_multiple(liste_val,tab_nom,superposition=np.array([]),nom_superposition="",message=""):
    #Si on a plus de 9 objets à afficher ce programme ne marche pas, on stoppe son
    #exécution et on affiche un message d'erreur
    if len(liste_val)>9 : 
        print("la fonction plot_multiple ne peut afficher plus de 9 plot en même temps")
        sys.exit()
        
    #calcul du nombre de lignes et de colonnes dont on a besoin pour afficher 
    #toutes les matrices
    if np.sqrt(len(tab_nom)) - int(np.sqrt(len(tab_nom))) >=0.5 : 
        nb_lignes=int(np.sqrt(len(tab_nom))) +1
        nb_col = nb_lignes  
    else :
        nb_lignes=int(np.sqrt(len(tab_nom)))
        nb_col = nb_lignes + 1   
    
    #initialisation de l'indice du subplot en fonction du nombre de lignes et 
    #de colonnes     
    ind = 100*nb_lignes + 10*nb_col +1
    plt.figure(figsize=(15,15))
    
    #affichage 
    for i in range(len(tab_nom)):
        plt.subplot(ind)
        plt.title(message + tab_nom[i])
        if len(superposition)!=0 : plt.plot(superposition,label=nom_superposition)
        plt.plot(liste_val[i],label=tab_nom[i])
        plt.legend(loc=0)
        ind += 1
    
    plt.show()
    
#-------------------------------------------------------------------------------   
 
 
#-------------------------------------------------------------------------------  
    
# Affiche le bed et la surface en 3D sur le même graphique   
def afficher_bed_surf(b,s,x,y):
    xi,yi = np.meshgrid(x, y)
        
    figb=plt.figure('3d',figsize=(15,5))
    ax = Axes3D(figb)
    surf=ax.plot_surface(xi,yi,b, cmap='ocean', linewidth=0, antialiased=False)
    surf2=ax.plot_surface(xi,yi,s, cmap='bwr', linewidth=0, antialiased=False)
    figb.colorbar(surf, shrink=0.5, aspect=5).set_label("bed")
    figb.colorbar(surf2, shrink=0.5, aspect=5).set_label("surf")
    
    plt.show()  

#-------------------------------------------------------------------------------   
 
 
#-------------------------------------------------------------------------------

# Deuxième version de la fonction précédente      
def afficher_v2_bed_surf(b,s,c,dh,x,y):
    xi,yi = np.meshgrid(x, y)

    cnorm=c/c.mean()
    dhnorm=dh/dh.mean()
    
    figb=plt.figure('3d',figsize=(15,5))
    ax = Axes3D(figb)
    surf=ax.plot_surface(xi,yi,b, facecolors=cm.jet(cnorm), linewidth=0, antialiased=False)
    surf2=ax.plot_surface(xi,yi,s, facecolors=cm.jet(dhnorm), linewidth=0, antialiased=False)
    figb.colorbar(surf, shrink=0.5, aspect=5).set_label("bed")
    figb.colorbar(surf2, shrink=0.5, aspect=5).set_label("surf")
    
    plt.show()
